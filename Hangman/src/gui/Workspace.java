package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import ui.AppGUI;
import javafx.scene.shape.*;
import java.io.IOException;
import java.util.ArrayList;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman'
    Button            hint;
    VBox guessBox;
    ArrayList<Node> graphics = makeGraphics();
    ArrayList<Node> rects;
    ArrayList<Node> letterss = makeLetters();
    HangmanController controller;
    Pane canvas;
    HBox box1;
    HBox box2;
    HBox box3;
    HBox box4;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        guiHeadingLabel.setFont(Font.font(50));
        headPane.setAlignment(Pos.CENTER);

        figurePane = new BorderPane();
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);

        bodyPane = new HBox();
        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        Pane wordBox = new Pane();

        startGame = new Button("Start Playing");
        hint = new Button("Hint");
        hint.setVisible(false);
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft,startGame,blankBoxRight);

        workspace = new GridPane();
        GridPane gamespace = new GridPane();

        Pane play = new VBox();
        canvas = initGraphics();
        for (Node node: graphics)
            node.setVisible(false);

        guessBox = new VBox();
        box1 = new HBox();
        box3 = new HBox();
        box4 = new HBox();
        box2 = new HBox();
        play.getChildren().addAll(bodyPane, wordBox, guessBox, hint);
        gamespace.add(play, 1, 0);
        gamespace.add(canvas, 0,0);
        GridPane.setHgrow(play, Priority.ALWAYS);
        GridPane.setHgrow(canvas, Priority.ALWAYS);

        workspace = new BorderPane();
        ((BorderPane)workspace).setTop(headPane);
        ((BorderPane)workspace).setCenter(gamespace);
        ((BorderPane)workspace).setBottom(footToolbar);


//        for (Shape node : graphics) {
//            workspace.getChildren().add(node);
//        }
    }

    public ArrayList<Node> getLetters (){
        return rects;
    }

    private ArrayList<Node> makeLetters() {
        rects = makeRects();
        ArrayList<Node> a = new ArrayList();
        for (int i = 0; i < 26; i++ ) {
            Pane b = new Pane();
            b.getChildren().add(rects.get(i));
            Text ch = new Text(17, 32, (char) (i + 65) + "");
            ch.setFont(Font.font("Century Gothic", 25));
            b.getChildren().add(ch);
            a.add(b);
        }
        return a;
    }

    private ArrayList<Node> makeRects(){
        ArrayList<Node> nodeArrayList = new ArrayList<>();
        for (int i = 0; i < 26; i++ ) {
            Rectangle c = new Rectangle(0, 0, 50, 50);
            nodeArrayList.add(c);
            c.setStroke(Color.BLACK);
            c.setFill(Color.IVORY);
        }
        return nodeArrayList;
    }

    public void resetGuessBox() {
        HBox a = new HBox();
        Rectangle b = new Rectangle(10,10,30,30);
        b.setVisible(false);
        a.getChildren().add(b);
        letterss = makeLetters();
        for (int i = 0; i < 7; i++)
            box1.getChildren().add(letterss.get(i));
        for (int i = 7; i < 14; i++)
            box2.getChildren().add(letterss.get(i));
        for (int i = 14; i < 21; i ++ )
            box3.getChildren().add(letterss.get(i));
        for (int i = 21; i < 26; i ++ )
            box4.getChildren().add(letterss.get(i));
        guessBox.getChildren().addAll(a,box1,box2,box3,box4);
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
        hint.setOnMouseClicked(e-> controller.hint());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public ArrayList<Node> getGraphics(){
        return graphics;
    }

    public ArrayList<Node> makeGraphics(){
        int x = 40;
        int y = 0;
        ArrayList<Node> human = new ArrayList<>();
        Line hanger1 = new Line(x,y, x + 200, y);
        hanger1.setStrokeWidth(5);
        Line hanger2 = new Line(x,y, x, 350 + y);
        hanger2.setStrokeWidth(5);
        Line hanger3 = new Line(x - 20,350 + y, 100 + x, 350 + y);
        hanger3.setStrokeWidth(5);
        Line hanger4 = new Line(200 + x, y, 200 + x, y + 20);
        hanger4.setStrokeWidth(5);
        Group hanger = new Group(hanger1, hanger2, hanger3, hanger4);
        Circle head = new Circle(x + 200, y + 90, 70);
        head.setStrokeWidth(3);
        Line body = new Line (200 + x, 160 + y, 200 + x, 250 + y);
        body.setStrokeWidth(3);
        Line arm1 = new Line (200 + x,160 + y, 120 + x, 200 + y);
        arm1.setStrokeWidth(3);
        Line arm2 = new Line (200 + x,160 + y, 280 + x, 200 + y);
        arm2.setStrokeWidth(3);
        Line leg1 = new Line(200 + x, 250 + y, 120 + x, 300 + y);
        leg1.setStrokeWidth(3);
        Line leg2 = new Line(200 + x, 250 + y, 280 + x, 300 + y);
        leg2.setStrokeWidth(3);
        head.setFill(null);
        head.setStroke(Color.BLACK);
        human.add(hanger3);
        human.add(hanger2);
        human.add(hanger1);
        human.add(hanger4);
        human.add(head);
        human.add(body);
        human.add(arm1);
        human.add(arm2);
        human.add(leg1);
        human.add(leg2);
        Circle eye1 = new Circle(x+175, y +90, 10);
        Circle eye2 = new Circle(x+225, y +90, 10);
        Arc mouth = new Arc(x+200, y+150, 225-175-30, 30, 0, 180);
        mouth.setFill(null);
        mouth.setStroke(Color.BLACK);
        human.add(eye1);
        human.add(eye2);
        human.add(mouth);
        return human;
    }

    public Pane initGraphics(){
        Pane canvas = new Pane();
        canvas.getChildren().addAll(graphics);

        return canvas;
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public Button getStartGame() {
        return startGame;
    }

    public Button getHint(){
        return hint;
    }

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        for (int i = 0; i < graphics.size(); i++)
            graphics.get(i).setVisible(false);
        hint.setVisible(false);
        hint.setDisable(false);
        guessBox.getChildren().clear();
        box1.getChildren().clear();
        box2.getChildren().clear();
        box3.getChildren().clear();
        box4.getChildren().clear();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);

    }

}
