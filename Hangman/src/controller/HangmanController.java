package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.w3c.dom.css.Rect;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Set;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private ArrayList<Node> progresss = new ArrayList<>();
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Button      hintButton;
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;
    private ArrayList<Node> graphics;
    private char[] targetword;
    private char[] letters;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;

    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
            hintButton = workspace.getHint();
            graphics = workspace.getGraphics();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        boolean done = false;
        while (!done) {
            gamedata.init();
            done = isOkay(gamedata.getTargetWord());
        }
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);
        for (int i = 0; i < graphics.size(); i++)
            graphics.get(i).setVisible(false);
        gameWorkspace.resetGuessBox();
        hintButton.setVisible(gamedata.difficult());
        hintButton.setDisable(gamedata.getHint());
        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        hintButton.setDisable(true);
        if (!success) {
            for (int i = 0; i < targetword.length; i++) {
                if ((!progress[i].isVisible())) {
                    progress[i].setVisible(true);
                    ((Rectangle)progresss.get(i)).setFill(Color.GRAY);
                }
            }
            graphics.get(10).setVisible(true);
            graphics.get(11).setVisible(true);
            graphics.get(12).setVisible(true);
        }
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }

    //Checks if word has no invalid symbols
    private boolean isOkay(String word){
        for (int i = 0; i < word.length(); i++)
            if (!Character.isLetter(word.charAt(i)))
                return false;
        return true;
    }
    private void initWordGraphics(HBox guessedLetters) {
        targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        Pane canvas = new Pane();

        progresss = new ArrayList<>();
        int x = 0;
        for (int i = 0; i < progress.length; i++) {
            Rectangle box = new Rectangle(x,0, 20 ,20);
            box.setFill(Color.BLACK);
            box.setStroke(Color.WHITE);
            canvas.getChildren().add(box);
            progresss.add(box);
            progress[i] = new Text(x+5, 15 ,Character.toString(targetword[i]).toUpperCase());
            progress[i].setFill(Color.WHITE);
            progress[i].setVisible(false);
            canvas.getChildren().add(progress[i]);

            x += 21;
        }

        for (int i = 0; i < graphics.size(); i++)
            graphics.get(i).setVisible(false);

        HBox wordbox = new HBox();
        wordbox.setSpacing(5);
        wordbox.getChildren().addAll(canvas);
        guessedLetters.getChildren().addAll(wordbox);
    }

    private void restoreGUI() {
        enableGameButton();
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        restoreWordGraphics(guessedLetters);

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        hintButton.setVisible(gamedata.difficult());
        hintButton.setDisable(gamedata.getHint());
        success = false;
        play();
    }

    private void restoreWordGraphics(HBox guessedLetters) {
        discovered = 0;
        targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
        workspace.resetGuessBox();
        ArrayList<Node> l = workspace.getLetters();
        Pane canvas = new Pane();

        progresss = new ArrayList<>();
        int x = 0;
        for (int i = 0; i < targetword.length; i++) {
            Rectangle box = new Rectangle(x,0, 20 ,20);
            box.setFill(Color.BLACK);
            box.setStroke(Color.WHITE);
            canvas.getChildren().add(box);
            progresss.add(box);
            progress[i] = new Text(x+5, 15 ,Character.toString(targetword[i]).toUpperCase());
            progress[i].setFill(Color.WHITE);
            progress[i].setVisible(gamedata.getGoodGuesses().contains(targetword[i]));
            canvas.getChildren().add(progress[i]);
            if (progress[i].isVisible()) {
                ((Rectangle)l.get(targetword[i] - 97)).setFill(Color.LIGHTSTEELBLUE);
                discovered++;
            }
            x += 21;
        }
        int r = 10 - gamedata.getRemainingGuesses();
        for (int i = 0; i < graphics.size(); i++) {
                graphics.get(i).setVisible(r > 0);
                r--;
        }

        for (char i = 97; i <= 122; i++){
            if (gamedata.getBadGuesses().contains(i))
                ((Rectangle)l.get(i-97)).setFill(Color.LIGHTSTEELBLUE);
        }

        HBox wordbox = new HBox();
        wordbox.setSpacing(5);
        wordbox.getChildren().addAll(canvas);
        guessedLetters.getChildren().addAll(wordbox);
    }

    public void play() {
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = Character.toLowerCase(event.getCharacter().charAt(0));
                    if (Character.isLetter(guess)) {
                        if (!alreadyGuessed(guess)) {
                            Workspace workspace = (Workspace)appTemplate.getWorkspaceComponent();
                            ArrayList<Node> l = workspace.getLetters();
                            Rectangle a = (Rectangle)l.get(guess - 97);
                            a.setFill(Color.LIGHTSTEELBLUE);
                            boolean goodguess = false;
                            for (int i = 0; i < progress.length; i++) {
                                if (gamedata.getTargetWord().charAt(i) == guess) {
                                    progress[i].setVisible(true);
                                    gamedata.addGoodGuess(guess);
                                    goodguess = true;
                                    discovered++;
                                }
                            }
                            if (!goodguess) {
                                gamedata.addBadGuess(guess);
                                graphics.get(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED - gamedata.getRemainingGuesses() - 1).setVisible(true);
                            }
                            success = (discovered == progress.length);
                            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                        }
                        setGameState(GameState.INITIALIZED_MODIFIED);
                    } else invalidSymbol();
                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void invalidSymbol() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        messageDialog.show("Invalid symbol", "Invalid symbol. Please only use letters A-Z");
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    public void hint(){
        hintButton.setDisable(true);
        boolean done = false;
        char[] t = gamedata.getTargetWord().toCharArray();
        while(!done) {
            int rand = ((int) (Math.random() * (t.length)));
            if(!gamedata.getGoodGuesses().contains(t[rand])) {
                for (int i = 0; i < targetword.length; i++) {
                    if (gamedata.getTargetWord().charAt(i) == t[rand]) {
                        progress[i].setVisible(true);
                        Workspace a =(Workspace)appTemplate.getWorkspaceComponent();
                        Rectangle b = (Rectangle)a.getLetters().get(t[rand] - 97);
                        b.setFill(Color.LIGHTSTEELBLUE);
                        gamedata.addGoodGuess(t[rand]);
                        discovered++;
                    }
                    done = true;
                }
            }
        }
        gamedata.usedHint();
        graphics.get(10 - gamedata.getRemainingGuesses()-1).setVisible(true);
        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
        success = (discovered == progress.length);
        gamestate = GameState.INITIALIZED_MODIFIED;
    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists()) {
                load(selectedFile.toPath());
                restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
            }
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
